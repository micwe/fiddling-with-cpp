#include <iostream>
#include <string>
#include <vector>
#include <cmath>
using namespace std ;


/* print out one-dimensional arrays.
 *
 *@param array : one-dimensional array
 */
void print_array(vector<float> array){
    int size = array.size() ;
    if (size < 50){
        cout << "[ " << array[0] ;
        for (int i = 1; i < size; ++i){
            cout << ", " << array[i];
            }
        cout << " ]";
        } else {
        cout << "[ " << array[0];
        for (int i=1; i<10; ++i){
            cout << ", " << array[i];
            }
        cout << ", \n... , ..., ... ";
        for (int i=size-10; i<size; ++i){
            cout << ", " << array[i];
            }
        cout << " ]";
         }
     }

/* Grid Class.
 * 
 * @attr xmin : minimal value of linear space.
 * @attr xmax : maximal value of linear space.
 * @attr num : number of elements in linear space.
*/
class linearGrid {
    public:
    float xmin;
    float xmax;
    float num;
    vector<float> coordinate;

    // Constructor
    linearGrid(float min, float max, int gpd) {
         xmin = min;
         xmax = max;
         num = floor(abs(xmin-xmax)*gpd);
    }

    // prints attributes of Grid class.
    void print_Grid() {
        cout << "\n<---- Grid class ---->";
        cout << "\nxmin = " << xmin;
        cout << "\nxmax = " << xmax;
        cout << "\nnum = " << num;
        }


    // creates linear space with given Grid class attributes
//    vector<float> linspace(float xmin, float xmax, int num){
    vector<float> linspace(){
         vector<float> x;
         float xi = 0 ; // xi: i-th element of a linspace x
         for (int i =0; i < num; i++){
             xi = xmin + i *(xmax-xmin)/(num-1);
             x.push_back(xi);
             }
//       vector<float> coordinate = x;
//         cout << "\n---> Create linear grid: done";
         return x;
         }
     } ;
