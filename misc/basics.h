#include <iostream>
using namespace std;
#include <string>
#include <vector>


/* linspace creates a linear one-dimensinal space.
 * Based on <vectors>.
 * 
 * @param xmin : minimal value of linear space.
 * @param xmax : maximal value of linear space.
 * @param num : number of elements in linear space.
 * @return x : linear space itself.
 */ 
vector<float> linspace(float xmin, float xmax, int num){
     vector<float> x;
     float xi = 0 ; // xi: i-th element of a linspace x
     for (int i =0; i < num; i++){
         xi = xmin + i *(xmax-xmin)/(num-1);
         x.push_back(xi);
         } 
     return x; 
     }

/* print out one-dimensional arrays (see linspace).
 *
 *@param array : one-dimensional array
 *@param show : True= shows array details
 */
void print_array(vector<float> array, bool show=false){
    int size = array.size();
    cout << "[ " << array[0] ;
    for (int i=1; i<size; i++){
        cout << ", "<<array[i];
        }
    cout << " ]";
    if (show == true){
        size = array.size();
        cout << "\n\n--------------\nArray details:\n........";
        cout << "\nnumber of elements: " << size;
        cout << "\nfirst value: " << array[0];
        cout << "\nlast value: " << array[size-1];
        cout << "\n\n--------------\n";
        }
    }

