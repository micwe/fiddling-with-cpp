#include <iostream>
#include <vector>
#include <string>

/**
 * Calculates the fibonacci series.
 * (recurrence relation)
 * F1 = 0, F2 = 1
 * F(n) = F(n-1) + F(n-2)
 * for all n > 1
 * @param number 
*/
std::vector<int> fibonacci_numbers(int number){
    int f1 = 0;
    int f2 = 1;
    int fn = 0;
    std::cout << f1 << ", ";
    std::cout << f2 << ", ";
    std::vector<int> fiboseries;
    for (int i = 2; i<number; ++i){
        fn = f2 + f1;
        f2 = fn;
        f1 = f2;
        fiboseries.push_back(fn);
        std::cout << fn << ", ";
    }
    return fiboseries;
}

int main(){
    int n = 10;
    fibonacci_numbers(n);
    return 0;
}