#include <iostream>
#include <string>
#include <vector>
#include <cmath>
using namespace std ;


/* 
 * Gaussian : 
 * f = exp(-a*x^2)
 * 
 * Parameter
 * ---------
 * coord : x-cooridnates (domain)
 * alpha : pre-factor
 *
 * Returns
 * -------
 * func : function values (range)
 * */
vector<float> gauss(vector<float> coord, float alpha){    
    int size = coord.size();
    float inpu ;
    vector<float> func;
    float ifunc = 0;
    for (int i=0; i<size; ++i){
	inpu = -alpha*pow(coord[i],2);
        ifunc = exp(inpu);
        func.push_back(ifunc);
        }
    return func;
    }

// TODO: install requirements for the use of hermitel()
// vector<float> hermite_polynom(vector<float> coord, int n){
//     int size = coord.size();
//     vector<float> func;
//     float ifunc = 0;
//     for (int i=0; i<size; ++i){
//         ifunc = hermitel(n, coord[i]);
//         func.push_back(ifunc);
//         }
//     return func;
//     }
