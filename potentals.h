#include <iostream>
#include <string>
#include <vector>
#include <cmath>
using namespace std ;


/*
 * Quantum mechanical harmonic potential function
 * 
 * Parameter
 * ---------
 * coord : Grid coordinates(domain)
 * k : quantum number
 *
 * Return
 * pot : potential function values (range)
 */
vector<float> harmonic_potential(vector<float> coord, float k){
    int size = coord.size();
    vector<float> pot;
    float ipot = 0;
    float prefac = 0.5*k;
    for (int i=0; i<size; ++i){
        ipot = prefac*pow(coord[i],2);
        pot.push_back(ipot);
        }
    return pot;
    }
