#include <iostream>
#include "Grid.h"
#include "potentals.h"
#include "basis.h"
using namespace std ; 

int main(){

    // create a linear grid 	
    linearGrid mygrid(-4,4,45) ;
    mygrid.print_Grid() ;
    vector<float> coord ;
    coord = mygrid.linspace();
    cout << "\n\nCreate Coordinate: done\n\nx=";
    print_array(coord);
    cout << "\n\nsize(coord)=" << coord.size() ;

    // create a potential function
    vector<float> pot ;
    pot = harmonic_potential(coord,1);
    cout << "\n\nHarmonic Potential:\npot=";
    print_array(pot);
    cout << "\n\n----- exponential function ------" ;
    vector<float> gau ;
    gau  = gauss(coord,1);
    cout << "\n\ngaussian=";
    print_array(gau);
//    cout << "\n\n----- hermite function ------" ;
//    vector<float> her ;
//    int n =  1 ;
//    her  = hermite_polynom(coord,n);
//    cout << "\n\nhermite=";
//    print_array(her);
return 0;
}
